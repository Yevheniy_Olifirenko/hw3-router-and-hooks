import React, { useState, useEffect } from 'react';
import Modal from '../Modal/ModalText';
import './CartPage.scss';

const CartPage = () => {
    const [cartItems, setCartItems] = useState([]);
    const [selectedProduct, setSelectedProduct] = useState(null);
    const [isModalOpen, setIsModalOpen] = useState(false);

    useEffect(() => {
        const storedCart = localStorage.getItem('cart');
        if (storedCart) {
            setCartItems(JSON.parse(storedCart));
        }
    }, []);

    const removeFromCart = (productId) => {
        setSelectedProduct(productId);
        setIsModalOpen(true);
    };

    const handleConfirmRemove = () => {
        const updatedCart = cartItems.filter((product) => product.article !== selectedProduct);
        setCartItems(updatedCart);
        localStorage.setItem('cart', JSON.stringify(updatedCart));

        setIsModalOpen(false);
    };

    const handleCancelRemove = () => {
        setIsModalOpen(false);
    };

    const calculateTotal = () => {
        if (!Array.isArray(cartItems) || cartItems.length === 0) {
            return 0;
        }

        const total = cartItems.reduce((acc, product) => acc + (parseFloat(product.price) || 0), 0);
        return isNaN(total) ? 0 : total;
    };

    return (
        <div className="cart-page">
            <div className="cart-header">
                <h2 className="cart-title">Shopping Cart</h2>
                <div className="cart-total">
                    Total: <strong>${calculateTotal()}</strong>
                </div>
            </div>

            <ul className="product-list">
                {Array.isArray(cartItems) && cartItems.length > 0 ? (
                    cartItems.map((product) => (
                        <li key={product.article} className="product-item">
                            <div className="product-info">
                                <p className="product-name">{product.name}</p>
                                <p className="product-price">₴{(parseFloat(product.price))}</p>
                            </div>
                            <button
                                className="remove-button"
                                onClick={() => removeFromCart(product.article)}
                            >
                                Remove
                            </button>
                        </li>
                    ))
                ) : (
                    <li key="empty-cart" className="empty-cart">
                        Your cart is empty.
                    </li>
                )}
            </ul>


            {/* Modal*/}
            <Modal
                isOpen={isModalOpen}
                onClose={handleCancelRemove}
                title="Remove Item"
                content="Are you sure you want to remove this item from the cart?"
                onCancel={handleCancelRemove}
                onConfirm={handleConfirmRemove}
            />
        </div>
    );
};

export default CartPage;
